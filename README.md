# cucumber-jvm-example

## Prerequisitos

### Instalar Java 8+

TBD

### Instalar Maven 3+

TBD

### Bajar WebDriver para el navegador que van a usar para testear en su máquina

En la sección de [Downloads de Selenium](https://www.seleniumhq.org/download/), bajo el título de
**Third Party Drivers, Bindings, and Plugins** se encuentran links para bajar los web drivers.

Algunos de ellos:
* [Firefox](https://github.com/mozilla/geckodriver/)
* [Chrome/Chromium](https://sites.google.com/a/chromium.org/chromedriver/downloads)

A modo práctico algunos de ellos ya están en este repositorio, pero no es la idea que estén versionados
junto con el código.

## Para correr las preubas

Para usar **Firefox** como web driver:

`mvn clean install -Dwebdriver=firefox`

Para usar **Chrome/Chromium** como web driver:

`mvn clean install -Dwebdriver=chrome`

**Nota**: Si solo se ejecuta `mvn clean install`, toma por defecto **Firefox**