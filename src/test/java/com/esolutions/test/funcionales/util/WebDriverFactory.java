package com.esolutions.test.funcionales.util;

import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebDriverFactory {

    public static final String WEB_DRIVER_CHROME = "chrome";
    public static final String WEB_DRIVER_FIREFOX = "firefox";

    private static final Logger LOGGER = LoggerFactory.getLogger(WebDriverFactory.class);
    private static final String WEB_DRIVER_PROPERTY_NAME = "webdriver";
    private static final String WEB_DRIVER_FIREFOX_PATH_PROPERTY_NAME = "webdriver.gecko.driver";
    private static final String WEB_DRIVER_FIREFOX_PATH_LINUX = "src/test/resources/webdrivers/geckodriver-v0.24.0-linux64";
    private static final String WEB_DRIVER_FIREFOX_PATH_WINDOWS = "src/test/resources/webdrivers/geckodriver-v0.24.0-win64.exe";
    private static final String WEB_DRIVER_CHROME_PATH_PROPERTY_NAME = "webdriver.chrome.driver";
    private static final String WEB_DRIVER_CHROME_PATH_LINUX = "src/test/resources/webdrivers/chromedriver-72.0.3626.69-linux64";
    private static final String WEB_DRIVER_CHROME_PATH_WINDOWS = "src/test/resources/webdrivers/chromedriver-74.0.3729.6-win32.exe";

    public static WebDriver getWebDriver() {

        String expectedWebDriver = System.getProperty(WEB_DRIVER_PROPERTY_NAME);

        LOGGER.info("El webdriver solicitado para realizar las pruebas es: {}",
                expectedWebDriver != null ? expectedWebDriver : "ninguno");

        if (expectedWebDriver == null) {
            LOGGER.info("Utilizando webdriver por defecto: Firefox");
            return initializeFirefoxWebDriver();
        } else {
            expectedWebDriver = expectedWebDriver.toLowerCase();
        }

        switch (expectedWebDriver) {
            case WEB_DRIVER_CHROME:
                return initializeChromeWebDriver();
            case WEB_DRIVER_FIREFOX:
            default:
                return initializeFirefoxWebDriver();
        }
    }

    private static WebDriver initializeFirefoxWebDriver() {
        LOGGER.info("Inicializando webdriver para Firefox");
        if (SystemUtils.IS_OS_LINUX) {
            System.setProperty(WEB_DRIVER_FIREFOX_PATH_PROPERTY_NAME, WEB_DRIVER_FIREFOX_PATH_LINUX);
        }
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty(WEB_DRIVER_FIREFOX_PATH_PROPERTY_NAME, WEB_DRIVER_FIREFOX_PATH_WINDOWS);
        }
        return new FirefoxDriver();
    }

    private static WebDriver initializeChromeWebDriver() {
        LOGGER.info("Inicializando webdriver para Chrome");
        if (SystemUtils.IS_OS_LINUX) {
            System.setProperty(WEB_DRIVER_CHROME_PATH_PROPERTY_NAME, WEB_DRIVER_CHROME_PATH_LINUX);
        }
        if (SystemUtils.IS_OS_WINDOWS) {
            System.setProperty(WEB_DRIVER_FIREFOX_PATH_PROPERTY_NAME, WEB_DRIVER_CHROME_PATH_WINDOWS);
        }
        return new ChromeDriver();
    }
}
