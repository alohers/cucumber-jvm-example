package com.esolutions.test.funcionales.steps;

import com.esolutions.test.funcionales.util.WebDriverFactory;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;

public class AgregarTareasALaLista {

    private static final String TODO_LIST_URL = "http://todomvc.com/examples/angular2/";

    private static Logger logger = LoggerFactory.getLogger(AgregarTareasALaLista.class);
    private WebDriver webDriver;

    @Before
    public void beforeScenario() {
        webDriver = WebDriverFactory.getWebDriver();
    }

    @After
    public void afterScenario() {
        if (webDriver != null) {
            webDriver.close();
        }
    }

    @Given("^una lista de tareas vacia$")
    public void una_lista_de_tareas_vacia() {
        logger.info("Abriendo URL: {}", TODO_LIST_URL);
        webDriver.navigate().to(TODO_LIST_URL);
    }

    @When("^agrego los siguientes (.+)$")
    public void agrego_los_siguientes_items(String items) {
        logger.info("Agregando el/los siguientes item/s {}", items);

        By inputSelector = By.cssSelector(".new-todo");

        WebDriverWait wait = new WebDriverWait(webDriver, 30);
        wait.until(new ExpectedCondition<Boolean>() {
            @NullableDecl
            @Override
            public Boolean apply(@NullableDecl WebDriver webDriver) {
                return webDriver.findElement(inputSelector).isDisplayed();
            }
        });

        String [] itemList = items.split(",");

        for (String item : itemList) {
            webDriver.findElement(inputSelector).sendKeys(item);
            webDriver.findElement(inputSelector).sendKeys(Keys.ENTER);
        }
    }

    @Then("^veo los (.+) agregados a la lista de tareas$")
    public void veo_los_items_agregados_a_la_lista_de_tareas(String items) {
        logger.info("Validando que el/los item/s '{}' son visibles", items);

        Pattern pattern = Pattern.compile(",");
        List<String> itemList = pattern.splitAsStream(items).map(item -> item.trim()).collect(Collectors.toList());

        By itemListSelector = By.cssSelector(".todo-list li label");
        List<WebElement> webItemList = webDriver.findElements(itemListSelector);

        for (WebElement webItem : webItemList) {
            String itemAdded = webItem.getText().trim();
            assertThat(itemList, hasItem(itemAdded));
        }
    }
}
