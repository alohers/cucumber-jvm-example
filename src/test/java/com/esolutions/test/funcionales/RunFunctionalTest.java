package com.esolutions.test.funcionales;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        plugin = { "pretty", "html:target/cucumber-report" }
)
public class RunFunctionalTest {
}
