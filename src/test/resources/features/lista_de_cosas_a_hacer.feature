Feature: Lista de cosas a hacer

  Para no olvidarme de las cosas que quiero hacer
  Como una persona responsable
  Quiero poder manejar una lista de cosas por hacer

  Por lo general tengo que manejar muchas tareas y muchas de ellas surgen en el mismo día.
  Si no mantengo esa información en algún lado (una libreta, etc.), suelo olvidarme de cosas que me
  comprometí a hacer.

    Scenario Template: Agregar tareas a la lista
      Given una lista de tareas vacia
      When agrego los siguientes <items>
      Then veo los <items> agregados a la lista de tareas
      Examples:
        | items                                                               |
        | Reunion Mensual                                                     |
        | Reunion Mensual, Story mapping de la AC102, Crear ambiente de test  |